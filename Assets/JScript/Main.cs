using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Main : MonoBehaviour
{
    public GameObject outsidebt;
    public GameObject outsidebt_1;
    public GameObject list;
    public GameObject list_1;
    public GameObject boy;
    public bool boymove;
    public GameObject videobt;
    public GameObject cam_2;
    public GameObject image_ob;
    public int once;
    public float time;
    //public Animator anim;
    public Animation animation_sc;
    public Animator cube_ui;

    public GameObject YoutubeAdvanced;
    public GameObject videoplane;
    public bool video_playing;
    //public GameObject progress_bg;

    //public float width;
    //public float height;
    //public float sc;
    public GameObject can;
    private YoutubePlayer _youtubePlayer;
    [SerializeField] private Image _imageLoading;

    private const int BET_HIDE_POS_X = 5000;

    //public GameObject pause_play_bt;

    // Start is called before the first frame update
    void Start()
    {
        //anim = boy.GetComponent<Animator>();
        animation_sc = boy.GetComponent<Animation>();
        //width = Screen.width;
        //height = Screen.height;
        //can.GetComponent<CanvasScaler>().referenceResolution = new Vector2(Screen.width, Screen.height);

        _youtubePlayer = YoutubeAdvanced.GetComponent<YoutubePlayer>();
        if (_youtubePlayer == null) {
            Debug.LogErrorFormat("_youtubePlayer not found!");
        }
        else {
            _youtubePlayer.OnVideoStarted.AddListener(() => {
                Debug.Log("OnVideoStarted");
                HideLoadingImage();
            });
            // player.OnYoutubeUrlAreReady.AddListener(customPlayer.Play);
        }

        StartCoroutine(PlayLoadingAnim());
        HideLoadingImage(); // hide in default
    }

    // Update is called once per frame
    void Update()
    {
        //videoplane.GetComponent<RectTransform>().localScale = new Vector3((float)Screen.width / Screen.height, (float)Screen.width / Screen.height, 1);
        //width = Screen.width;
        //height = Screen.height;
        //sc = height / width;

        //plane.transform.localScale = new Vector3(width/1000, 1, height / 1000);

        if (image_ob != null && image_ob.activeSelf)
        {
            if (once == 0)
            {
                time = image_ob.GetComponent<AudioSource>().clip.length;
                boy.transform.SetParent(image_ob.gameObject.transform);
                boymove = true;

                if (image_ob.name == "ant" || image_ob.name == "hat" || image_ob.name == "jug" || image_ob.name == "queen" || image_ob.name == "sun" || image_ob.name == "zipper")
                {
                    animation_sc.CrossFade("happy", 0.5f);
                    //anim.SetTrigger("happy");
                    boy.GetComponent<Animator_sc>().org_number = 5;
                    boy.GetComponent<Animator_sc>().random_number = 5;
                    if (image_ob.name == "ant" || image_ob.name == "zipper")
                    {
                        outsidebt_1.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 200);
                        list.SetActive(false);
                    }
                    else
                    {
                        outsidebt.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 200);
                        list_1.SetActive(false);
                    }
                    cube_ui.Play("cube");
                }
                if (image_ob.name == "bed" || image_ob.name == "kite" || image_ob.name == "tiger")
                {
                    animation_sc.CrossFade("talking", 0.5f);
                    //anim.SetTrigger("talking");
                    boy.GetComponent<Animator_sc>().org_number = 4;
                    boy.GetComponent<Animator_sc>().random_number = 4;
                    outsidebt.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 200);
                    cube_ui.Play("cube");
                    list_1.SetActive(false);
                }
                if (image_ob.name == "cat" || image_ob.name == "fork" || image_ob.name == "lion" || image_ob.name == "octopus" || image_ob.name == "umbrella" || image_ob.name == "x-ray")
                {
                    animation_sc.CrossFade("ok", 0.5f);
                    //anim.SetTrigger("ok");
                    boy.GetComponent<Animator_sc>().org_number = 3;
                    boy.GetComponent<Animator_sc>().random_number = 3;
                    if (image_ob.name == "cat" || image_ob.name == "lion" || image_ob.name == "octopus" || image_ob.name == "umbrella" || image_ob.name == "x-ray")
                    {
                        outsidebt_1.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 200);
                        list.SetActive(false);
                    }
                    else
                    {
                        outsidebt.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 200);
                        list_1.SetActive(false);
                    }
                    cube_ui.Play("cube");
                }
                if (image_ob.name == "dog" || image_ob.name == "ink" || image_ob.name == "moon" || image_ob.name == "rabbit" || image_ob.name == "van")
                {
                    animation_sc.CrossFade("thinking", 0.5f);
                    //anim.SetTrigger("thinking");
                    boy.GetComponent<Animator_sc>().org_number = 2;
                    boy.GetComponent<Animator_sc>().random_number = 2;
                    if (image_ob.name == "ink" || image_ob.name == "rabbit")
                    {
                        outsidebt_1.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 200);
                        list.SetActive(false);
                    }
                    else
                    {
                        outsidebt.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 200);
                        list_1.SetActive(false);
                    }
                    cube_ui.Play("cube");
                }
                if (image_ob.name == "egg" || image_ob.name == "pig" || image_ob.name == "nest" || image_ob.name == "goat" || image_ob.name == "watch" || image_ob.name == "yolk")
                {
                    animation_sc.CrossFade("Touchbelly", 0.5f);
                    //anim.SetTrigger("Touchbelly");
                    boy.GetComponent<Animator_sc>().org_number = 1;
                    boy.GetComponent<Animator_sc>().random_number = 1;
                    if (image_ob.name == "egg")
                    {
                        outsidebt_1.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 200);
                        list.SetActive(false);
                    }
                    else
                    {
                        outsidebt.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 200);
                        list_1.SetActive(false);
                    }
                    cube_ui.Play("cube");
                }
                once = 1;
            }
            if (boymove)
            {
                time = time - 1 * Time.deltaTime;
            }
        }
        if (image_ob != null && image_ob.activeSelf == false && once == 1 && video_playing == false)
        {
            time = 0;
            boy.GetComponent<Animator_sc>().org_number = 0;
            boy.GetComponent<Animator_sc>().random_number = 0;
            outsidebt.GetComponent<RectTransform>().anchoredPosition = new Vector2(BET_HIDE_POS_X, 200);
            outsidebt_1.GetComponent<RectTransform>().anchoredPosition = new Vector2(BET_HIDE_POS_X, 200);
            image_ob = null;
            once = 0;
            cube_ui.Play("cube 0");
            boy.GetComponent<Animator_sc>().animation_time = 0;
            list.SetActive(true);
            list_1.SetActive(true);
        }
        if (time < 0)
        {
            boymove = false;
            animation_sc.CrossFade("Standby", 0.5f);
            //anim.SetTrigger("Standby");
            time = 0;
        }
    }

    private IEnumerator PlayLoadingAnim() {
        yield return null;

        if (_imageLoading != null) {
            while (true) {
                _imageLoading.GetComponent<RectTransform>().Rotate(0f, 0f, -20f, Space.Self);
                yield return new WaitForSeconds(0.05f);
            }
        }
    }

    private void ShowLoadingImage() {
        if (_imageLoading != null) {
            _imageLoading.gameObject.SetActive(true);
        }
    }

    private void HideLoadingImage() {
        if (_imageLoading != null) {
            _imageLoading.gameObject.SetActive(false);
        }
    }

    public void closebt()
    {
        outsidebt.GetComponent<RectTransform>().anchoredPosition = new Vector2(BET_HIDE_POS_X, 200);
        outsidebt_1.GetComponent<RectTransform>().anchoredPosition = new Vector2(BET_HIDE_POS_X, 200);
        Screen.orientation = ScreenOrientation.Portrait;
        videoplane.GetComponent<RawImage>().color = new Color(255, 255, 255, 0);
        videoplane.GetComponent<RectTransform>().localScale = new Vector3(0.5625f,0.5625f, 1);
    }
    public void closeapp()
    {
        Application.Quit();
    }
    public void pause_play(int xx)
    {
        if (xx == 0)
        {
            _youtubePlayer.Pause();
        }
        if (xx == 1)
        {
            _youtubePlayer.Play();
        }
        if (xx == 2)
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            videoplane.GetComponent<RectTransform>().localScale = new Vector3(1.7778f, 1.7778f, 1);
            //StartCoroutine(showVideo(1.78f));
        }
        if (xx == 3)
        {
            Screen.orientation = ScreenOrientation.Portrait;
            videoplane.GetComponent<RectTransform>().localScale = new Vector3(0.5625f, 0.5625f, 1);
            //StartCoroutine(showVideo(1f));
        }
    }
    IEnumerator showVideo(float xx)
    {
        yield return new WaitForEndOfFrame();
        can.GetComponent<CanvasScaler>().scaleFactor = xx;
    }
    public void closevideo()
    {
        list.SetActive(true);
        list_1.SetActive(true);
        once = 0;
        time = 0;
        boy.GetComponent<Animator_sc>().org_number = 0;
        boy.GetComponent<Animator_sc>().random_number = 0;
        outsidebt.GetComponent<RectTransform>().anchoredPosition = new Vector2(BET_HIDE_POS_X, 200);
        outsidebt_1.GetComponent<RectTransform>().anchoredPosition = new Vector2(BET_HIDE_POS_X, 200);
        image_ob = null;
        cube_ui.Play("cube 0");
        video_playing = false;
        _youtubePlayer.youtubeUrl = "";
        boy.GetComponent<Animator_sc>().animation_time = 0;
        videoplane.SetActive(false);
        Screen.orientation = ScreenOrientation.Portrait;
        videoplane.GetComponent<RawImage>().color = new Color(255, 255, 255, 0);
        videoplane.GetComponent<RectTransform>().localScale = new Vector3(0.5625f, 0.5625f, 1);
    }
    IEnumerator showVideo()
    {
        yield return new WaitForSeconds(2);
        videoplane.GetComponent<RawImage>().color = new Color(255, 255, 255, 255);
    }

    private string getVideoName(string imageName, int number) {
        var result = string.Empty;
        if (imageName == "ant")
        {
            switch (number) {
                case 0: result = "rAbbq8LpPNc"; break;
                case 1: result = "4siBtHM4F4g"; break;
                case 2: result = "82KZYekjnjM"; break;
                case 3: result = "eZtuQvLQr2s"; break;
            }
        }
        else if (imageName == "bed")
        {
            switch (number) {
                case 0: result = "JNRaa2pGehk"; break;
                case 1: result = "iyroszyC_e0"; break;
                case 2: result = "MRMuXaZ0cRo"; break;
            }
        }
        else if (imageName == "cat")
        {
            switch (number) {
                case 0: result = "nwmQH7d4dbU"; break;
                case 1: result = "vldZyJXeJMI"; break;
                case 2: result = "LQ6emFlqEVw"; break;
                case 3: result = "4NI74mjlPus"; break;
            }
        }
        else if (imageName == "dog")
        {
            switch (number) {
                case 0: result = "DhJE12U0e_Q"; break;
                case 1: result = "TmY2rixmNv4"; break;
                case 2: result = "Revyw8jR9DY"; break;
            }
        }
        else if (imageName == "egg")
        {
            switch (number) {
                case 0: result = "oL2QGvvu3B8"; break;
                case 1: result = "N7nRutCBtWQ"; break;
                case 2: result = "N6_-gUvjiLw"; break;
                case 3: result = "_GPp4RdMvug"; break;
            }
        }
        else if (imageName == "fork")
        {
            switch (number) {
                case 0: result = "3cA5RWiqCpE"; break;
                case 1: result = "cFLH8T3ubRE"; break;
                case 2: result = "xdxFQwHAjCE"; break;
            }
        }
        else if (imageName == "goat")
        {
            switch (number) {
                case 0: result = "5DrkdMRv6wo"; break;
                case 1: result = "GWhqM1Gcw7c"; break;
                case 2: result = "ZrXHwz0o2vc"; break;
            }
        }
        else if (imageName == "hat")
        {
            switch (number) {
                case 0: result = "fY5o2Ifm7Hk"; break;
                case 1: result = "lDdPXElqC6M"; break;
                case 2: result = "Lma3j0R0SzA"; break;
            }
        }
        else if (imageName == "ink")
        {
            switch (number) {
                case 0: result = "oSTaG_xGl9o"; break;
                case 1: result = "ZLcLvKKc4NQ"; break;
                case 2: result = "ej7UeAuiomA"; break;
                case 3: result = "zEAeXo7WaGQ"; break;
            }
        }
        else if (imageName == "jug")
        {
            switch (number) {
                case 0: result = "NklARUA8gZQ"; break;
                case 1: result = "iE4bRuJs7vQ"; break;
                case 2: result = "lTT1IXs_ZFA"; break;
            }
        }
        else if (imageName == "kite")
        {
            switch (number) {
                case 0: result = "g842PJX03ks"; break;
                case 1: result = "HPwq5zEiYJo"; break;
                case 2: result = "18WGYM6x3LU"; break;
            }
        }
        else if (imageName == "lion")
        {
            switch (number) {
                case 0: result = "l2qpnqZXF8o"; break;
                case 1: result = "HW3kO9FOJAM"; break;
                case 2: result = "YqSzyFw23C0"; break;
                case 3: result = "EzfOlxQynJg"; break;
            }
        }
        else if (imageName == "moon")
        {
            switch (number) {
                case 0: result = "SyEYE8yidUw"; break;
                case 1: result = "u1PhBknfYww"; break;
                case 2: result = "LjEFaKg6RYg"; break;
            }
        }
        else if (imageName == "nest")
        {
            switch (number) {
                case 0: result = "K97ADLTuhhM"; break;
                case 1: result = "nL9m6vSu32o"; break;
                case 2: result = "qfVhgdB_pF8"; break;
            }
        }
        else if (imageName == "octopus")
        {
            switch (number) {
                case 0: result = "aTurhASSRL4"; break;
                case 1: result = "ygO8ocfoU80"; break;
                case 2: result = "Ch2cvB-f6Pg"; break;
                case 3: result = "J0NrRT7UTv8"; break;
            }
        }
        else if (imageName == "pig")
        {
            switch (number) {
                case 0: result = "S9_znmgdG_A"; break;
                case 1: result = "iv7jmL1JWv4"; break;
                case 2: result = "RhkVCR38Zro"; break;
            }
        }
        else if (imageName == "queen")
        {
            switch (number) {
                case 0: result = "dDebNFUpSJ4"; break;
                case 1: result = "hQGpQ_GwPdQ"; break;
                case 2: result = "_Gx9O08cNWM"; break;
            }
        }
        else if (imageName == "rabbit")
        {
            switch (number) {
                case 0: result = "lTr2tqsOkD0"; break;
                case 1: result = "Xsb-ETUXvPU"; break;
                case 2: result = "97hhIK0UAxE"; break;
                case 3: result = "BUlX1O5zoro"; break;
            }
        }
        else if (imageName == "sun")
        {
            switch (number) {
                case 0: result = "b9DEOdnO1jE"; break;
                case 1: result = "zPd3FAAX9tI"; break;
                case 2: result = "uueVAHLIMSk"; break;
            }
        }
        else if (imageName == "tiger")
        {
            switch (number) {
                case 0: result = "Xi76Q-TPRzM"; break;
                case 1: result = "tP4bGzv-OGc"; break;
                case 2: result = "j53aFPRzxvE"; break;
            }
        }
        else if (imageName == "umbrella")
        {
            switch (number) {
                case 0: result = "wRsrWv_ty_Y"; break;
                case 1: result = "rt8PEBKTV5o"; break;
                case 2: result = "gCLbx0XyRkQ"; break;
                case 3: result = "Jqfs9R1adjU"; break;
            }
        }
        else if (imageName == "van")
        {
            switch (number) {
                case 0: result = "MY-ZxlZzOFI"; break;
                case 1: result = "rpbQvomBex0"; break;
                case 2: result = "os2naEn0v7M"; break;
            }
        }
        else if (imageName == "watch")
        {
            switch (number) {
                case 0: result = "Kn7nj7-ITg4"; break;
                case 1: result = "oebd7E0mPq8"; break;
                case 2: result = "Q9lT1UyKIxI"; break;
            }
        }
        else if (imageName == "x-ray")
        {
            switch (number) {
                case 0: result = "A_gILv4-2sU"; break;
                case 1: result = "XOz1MnJS63M"; break;
                case 2: result = "TntFXY_ovCQ"; break;
                case 3: result = "TIS64OWe1gc"; break;
            }
        }
        else if (imageName == "yolk")
        {
            switch (number) {
                case 0: result = "4EFdAfJ2yF0"; break;
                case 1: result = "f7d6B9cLgdg"; break;
                case 2: result = "-GiUTqs7rUM"; break;
            }
        }
        else if (imageName == "zipper")
        {
            switch (number) {
                case 0: result = "smCkn0via_I"; break;
                case 1: result = "4pModTjwrKw"; break;
                case 2: result = "87C6JvPKRuw"; break;
                case 3: result = "oBNd5GIhjSg"; break;
            }
        }

        return result;
    }

    public void yturl_fn(int xx)
    {
        const string URL_BASE = "https://youtu.be/";
 
        video_playing = true;
        //videobt.SetActive(false);

        var videoName = getVideoName(image_ob.name, xx);
        if (!string.IsNullOrEmpty(videoName)) {
            // StartCoroutine(_youtubePlayer.AsyncClearThenPlay(URL_BASE + videoName));
            _youtubePlayer.Play(URL_BASE + videoName);
            ShowLoadingImage();
        }
        else {
            Debug.LogErrorFormat("video name is empty! imageName:{0} number:{1}", image_ob.name, xx);
        }     

        // if (image_ob.name == "ant")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/rAbbq8LpPNc");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/4siBtHM4F4g");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/82KZYekjnjM");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/eZtuQvLQr2s");
        //     }
        // }
        // if (image_ob.name == "bed")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/JNRaa2pGehk");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/iyroszyC_e0");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/MRMuXaZ0cRo");
        //     }
        // }
        // if (image_ob.name == "cat")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/nwmQH7d4dbU");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/vldZyJXeJMI");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/LQ6emFlqEVw");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/4NI74mjlPus");
        //     }
        // }
        // if (image_ob.name == "dog")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/DhJE12U0e_Q");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/TmY2rixmNv4");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/Revyw8jR9DY");
        //     }
        // }
        // if (image_ob.name == "egg")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/oL2QGvvu3B8");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/N7nRutCBtWQ");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/N6_-gUvjiLw");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/_GPp4RdMvug");
        //     }
        // }
        // if (image_ob.name == "fork")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/3cA5RWiqCpE");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/cFLH8T3ubRE");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/xdxFQwHAjCE");
        //     }
        // }
        // if (image_ob.name == "goat")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/5DrkdMRv6wo");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/GWhqM1Gcw7c");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/ZrXHwz0o2vc");
        //     }
        // }
        // if (image_ob.name == "hat")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/fY5o2Ifm7Hk");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/lDdPXElqC6M");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/Lma3j0R0SzA");
        //     }
        // }
        // if (image_ob.name == "ink")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/oSTaG_xGl9o");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/ZLcLvKKc4NQ");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/ej7UeAuiomA");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/zEAeXo7WaGQ");
        //     }
        // }
        // if (image_ob.name == "jug")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/NklARUA8gZQ");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/iE4bRuJs7vQ");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/lTT1IXs_ZFA");
        //     }
        // }
        // if (image_ob.name == "kite")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/g842PJX03ks");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/HPwq5zEiYJo");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/18WGYM6x3LU");
        //     }
        // }
        // if (image_ob.name == "lion")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/l2qpnqZXF8o");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/HW3kO9FOJAM");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/YqSzyFw23C0");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/EzfOlxQynJg");
        //     }
        // }
        // if (image_ob.name == "moon")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/SyEYE8yidUw");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/u1PhBknfYww");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/LjEFaKg6RYg");
        //     }
        // }
        // if (image_ob.name == "nest")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/K97ADLTuhhM");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/nL9m6vSu32o");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/qfVhgdB_pF8");
        //     }
        // }
        // if (image_ob.name == "octopus")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/aTurhASSRL4");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/ygO8ocfoU80");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/Ch2cvB-f6Pg");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/J0NrRT7UTv8");
        //     }
        // }
        // if (image_ob.name == "pig")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/S9_znmgdG_A");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/iv7jmL1JWv4");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/RhkVCR38Zro");
        //     }
        // }
        // if (image_ob.name == "queen")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/dDebNFUpSJ4");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/hQGpQ_GwPdQ");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/_Gx9O08cNWM");
        //     }
        // }
        // if (image_ob.name == "rabbit")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/lTr2tqsOkD0");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/Xsb-ETUXvPU");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/97hhIK0UAxE");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/BUlX1O5zoro");
        //     }
        // }
        // if (image_ob.name == "sun")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/b9DEOdnO1jE");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/zPd3FAAX9tI");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/uueVAHLIMSk");
        //     }
        // }
        // if (image_ob.name == "tiger")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/Xi76Q-TPRzM");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/tP4bGzv-OGc");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/j53aFPRzxvE");
        //     }
        // }
        // if (image_ob.name == "umbrella")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/wRsrWv_ty_Y");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/rt8PEBKTV5o");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/gCLbx0XyRkQ");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/Jqfs9R1adjU");
        //     }
        // }
        // if (image_ob.name == "van")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/MY-ZxlZzOFI");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/rpbQvomBex0");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/os2naEn0v7M");
        //     }
        // }
        // if (image_ob.name == "watch")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/Kn7nj7-ITg4");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/oebd7E0mPq8");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/Q9lT1UyKIxI");
        //     }
        // }
        // if (image_ob.name == "x-ray")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/A_gILv4-2sU");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/XOz1MnJS63M");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/TntFXY_ovCQ");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/TIS64OWe1gc");
        //     }
        // }
        // if (image_ob.name == "yolk")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/4EFdAfJ2yF0");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/f7d6B9cLgdg");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/-GiUTqs7rUM");
        //     }
        // }
        // if (image_ob.name == "zipper")
        // {
        //     if (xx == 0)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/smCkn0via_I");
        //     }
        //     if (xx == 1)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/4pModTjwrKw");
        //     }
        //     if (xx == 2)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/87C6JvPKRuw");
        //     }
        //     if (xx == 3)
        //     {
        //         YoutubeAdvanced.GetComponent<YoutubePlayer>().PreLoadVideo("https://youtu.be/oBNd5GIhjSg");
        //     }
        // }

        videoplane.SetActive(true);
        StartCoroutine(showVideo());
    }
}
