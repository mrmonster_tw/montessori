﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animator_sc : MonoBehaviour
{
    public int org_number;
    public int random_number;
    public GameObject main;
    //public Animator anim;
    Animation animation_sc;
    public float animation_time;
    // Start is called before the first frame update
    void Start()
    {
        main = GameObject.FindGameObjectWithTag("main");
        //anim = main.GetComponent<Main>().boy.GetComponent<Animator>();
        animation_sc = main.GetComponent<Main>().boy.GetComponent<Animation>();
        animation_time = animation_sc.clip.length;
    }

    // Update is called once per frame
    void Update()
    {
        if (main.GetComponent<Main>().boymove)
        {
            animation_time = animation_time - 1 * Time.deltaTime;
        }
        if (main.GetComponent<Main>().boymove == false)
        {
            animation_time = 0;
        }

        if (animation_time < 0)
        {
            ani_end();
        }
    }
    public void ani_end()
    {
        random_number = Random.RandomRange(1, 6);
        if (random_number == org_number)
        {
            ani_end();
        }
        if (random_number != org_number)
        {
            ran_fn(random_number);
        }
    }
    void ran_fn(int xx)
    {
        if (xx == 1)
        {
            animation_sc.CrossFade("Touchbelly", 0.5f);
            animation_sc.clip = animation_sc.GetClip("Touchbelly");
            //anim.SetTrigger("Touchbelly");
            animation_time = animation_sc.clip.length;
        }
        if (xx == 2)
        {
            animation_sc.CrossFade("thinking", 0.5f);
            animation_sc.clip = animation_sc.GetClip("thinking");
            //anim.SetTrigger("thinking");
            animation_time = animation_sc.clip.length;
        }
        if (xx == 3)
        {
            animation_sc.CrossFade("ok", 0.5f);
            animation_sc.clip = animation_sc.GetClip("ok");
            //anim.SetTrigger("ok");
            animation_time = animation_sc.clip.length;
        }
        if (xx == 4)
        {
            animation_sc.CrossFade("talking", 0.5f);
            animation_sc.clip = animation_sc.GetClip("talking");
            //anim.SetTrigger("talking");
            animation_time = animation_sc.clip.length;
        }
        if (xx == 5)
        {
            animation_sc.CrossFade("happy", 0.5f);
            animation_sc.clip = animation_sc.GetClip("happy");
            //anim.SetTrigger("happy");
            animation_time = animation_sc.clip.length;
        }
        org_number = random_number;
    }
}
