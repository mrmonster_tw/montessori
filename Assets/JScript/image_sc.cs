﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class image_sc : MonoBehaviour
{
    public GameObject main;
    // Start is called before the first frame update
    void Start()
    {
        main = GameObject.FindGameObjectWithTag("main");
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.activeSelf)
        {
            main.GetComponent<Main>().image_ob = gameObject;
        }
    }
}
