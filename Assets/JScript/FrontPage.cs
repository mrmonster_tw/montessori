﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class FrontPage : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(OpenCamera());
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void NextScene()
    {
        Application.LoadLevelAsync("1");
    }
    private IEnumerator OpenCamera()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (!Application.HasUserAuthorization(UserAuthorization.WebCam)) yield break;
        WebCamDevice[] devices = WebCamTexture.devices;
    }
}
